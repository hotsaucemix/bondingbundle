﻿create table person(
    id serial primary key,
    fullName varchar(64) not null,
    contactNumber varchar(15) not null,
    contactEmail varchar(32) not null
);

create index person_idx on person(id);

create table client(
    clientId serial primary key,
    userName varchar(32) not null,
    passWord varchar(32) not null,
    location point not null
) inherits (person);

create table broker(
    brokerId serial primary key,
    brokerName varchar(32) not null,
    brokerPasswd varchar(32) not null,
    homePage varchar(32) default null
) inherits (person);

create table request(
    id serial primary key,
    clientId integer references client(clientId) on delete restrict,
    budget numeric(8,2) not null,
    capacity integer default 1 not null,
    startDate date default null,
    endDate date default null,
    details text default null
);

create index request_idx on request(id);

create table quote(
    id serial primary key,
    brokerId integer references broker(brokerId) on delete restrict,
    budget numeric(8,2) not null,
    capacity integer default 1 not null,
    location point not null,
    optNotes text default null
);

create index quote_idx on quote(id);

CREATE TYPE rateStar AS ENUM ('1', '2', '3', '4', '5');

create table Rating(
    id serial primary key,
    brokerId integer references broker(brokerId) on delete restrict,
    brokerRating rateStar not null,
    customerId integer references client(clientId) on delete restrict,
    ratingCommend text default null
);

create index rating_idx on rating(id);