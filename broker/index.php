﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Broker quote form</h3>
                </div>
				<div class="panel-body">
                    <div class="col-sm-6">
                            <form method="POST" onSubmit="<?php echo $_SERVER['PHP_SELF'];?>">
                                <div class="form-group">
                                    <label for="budget">Budget amount</label>
                                    <input type="number" class="form-control" id="budget" name="budget" placeholder="Enter budget">
                                </div>
                                <div class="form-group">
                                    <label for="capacity">Capacity</label>
                                    <input type="number" class="form-control" id="capacity" name="capacity" placeholder="Enter number of group members">
                                </div>
                                <div class="form-group">
                                    <label for="details">Details</label>
                                    <textarea class="form-control" rows="5" id="comment" name="details"placeholder="Enter additional details"></textarea>
                                </div>                                
                                <input type="submit" name="Find quote" id="Search" class="btn btn-info" onClick="document.pressed=this.value" value="Find request">
                            </form>
                    </div>
                    <div class="col-sm-6">
                        <h3>Please fill out the form properly for better match with client requests.</h3>
                    </div>
                </div>
            </div>
<?php
require_once('dbconn.php');

if(isset($_REQUEST["budget"]) || isset($_REQUEST["capacity"]))
{
    $budget= $_REQUEST["budget"];
    $capacity= $_REQUEST["capacity"];
    $details= $_REQUEST["details"];
    
    $saveQuery= "insert into quote(brokerId, budget, capacity, location, optNotes) values(1, '$budget', '$capacity', '(14.5887802,120.9749356)', '$details')";
    $stmt0= $dbh->query($saveQuery);

    $findQuery= "select budget, capacity, details from request order by id asc";
	$stmt1= $dbh->query($findQuery);
	$result1= $stmt1->fetchAll();

    if(sizeof($result1) > 0)
    {
?>
				<div class="list-group" role="navigation">
<?php	
		foreach($result1 as $row)
		{
?>
					<a href="#">Budget: <?php echo $row[0];?><br/>Capacity: <?php echo $row[1];?><br/>Notes: <?php echo $row[2];?></a><br/>
                    <br/>
<?php
		}
?>
				</div>
<?php		
	}
}

require_once('template/footer.php');               