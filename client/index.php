﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Client request form</h3>
                </div>
				<div class="panel-body">
                    <div class="col-sm-6">
                            <form method="POST" onSubmit="<?php echo $_SERVER['PHP_SELF'];?>">
                                <div class="form-group">
                                    <label for="budget">Budget amount</label>
                                    <input type="number" class="form-control" id="budget" name="budget" placeholder="Enter budget">
                                </div>
                                <div class="form-group">
                                    <label for="capacity">Capacity</label>
                                    <input type="number" class="form-control" id="capacity" name="capacity" placeholder="Enter number of group members">
                                </div>
                                <div class="form-group">
                                    <label for="details">Details</label>
                                    <textarea class="form-control" rows="5" id="comment" name="details"placeholder="Enter additional details"></textarea>
                                </div>                                
                                <input type="submit" name="Find quote" id="Search" class="btn btn-info" onClick="document.pressed=this.value" value="Find quote">
                            </form>
                    </div>
                    <div class="col-sm-6">
                        <h3>Please fill out the form properly for better match with product and service quotes.</h3>
                    </div>
                </div>
            </div>
<?php
require_once('dbconn.php');

if(isset($_REQUEST["budget"]) || isset($_REQUEST["capacity"]))
{
    $budget= $_REQUEST["budget"];
    $capacity= $_REQUEST["capacity"];
    $details= $_REQUEST["details"];
 
    $saveQuery= "insert into request(clientId, budget, capacity, details) values(1, '$budget', '$capacity', '$details')";
    $stmt0= $dbh->query($saveQuery);
    
    //SMS sending procedure causing system not to work as expected, will be commented out for now
    /*
    $messageId= uniqid(time(), true);    
    //after save into DB, should SMS request to brokers, this time hardwired to a single number
    $arr_post_body = array(
        "message_type" => "SEND",
        "mobile_number" => "639166402020",
        "shortcode" => "292903343",
        "message_id" => "$messageId",
        "message" => urlencode("Budget: $budget, capacity: $capacity, details: $details"),
        "client_id" => "3a312dd207fb64c33d1fd5c390f3b7f8d3ac8c31f710df3857617ba469b979fc",
        "secret_key" => "f26826841d4ec604228de33df7971f73b001d1d6299422f1de65f8a9af064daf"
    );

    $query_string = "";
    foreach($arr_post_body as $key => $frow)
    {
        $query_string .= '&'.$key.'='.$frow;
    }

    $URL = "http://post.chikka.com/smsapi/request";

    $curl_handler = curl_init();
    curl_setopt($curl_handler, CURLOPT_URL, $URL);
    curl_setopt($curl_handler, CURLOPT_POST, count($arr_post_body));
    curl_setopt($curl_handler, CURLOPT_POSTFIELDS, $query_string);
    curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, TRUE);
    $response = curl_exec($curl_handler);
    curl_close($curl_handler);

    //exit(0);    
    */

    //$findQuery= "select budget, capacity, location, optnotes from quote where budget <='$budget' and capacity>='$capacity' order by id asc";
    $findQuery= "select budget, capacity, location, optnotes from quote order by id asc";
	$stmt1= $dbh->query($findQuery);
	$result1= $stmt1->fetchAll();

    if(sizeof($result1) > 0)
    {
?>
				<div class="list-group" role="navigation">
<?php	
		foreach($result1 as $row)
		{
?>
					<a href="#">Budget: <?php echo $row[0];?><br/>Capacity: <?php echo $row[1];?><br/>Notes: <?php echo $row[3];?></a><br/>
                    <br/>
<?php
		}
?>
				</div>
<?php		
	}
}

require_once('template/footer.php');               